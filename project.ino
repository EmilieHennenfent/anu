//Musiques

//Mario 
const int noteCount = 111;
int melody[noteCount]={659,659,659,523,659,784,0,392,0,523,392,330,440,494,466,440,392,659,784,880,698,784,659,523,587,494,523,392,330,440,494,466,440,392,659,784,880,698,784,659,523,587,494,784,740,698,587,659,392, 440,523, 440,523,587, 784,740,698, 587,659, 1046,1046,1046,0,784,740,698, 587,659, 392,440,523,440,523,587, 622,587,523,0,523,523,523, 523,587,659,523,440,392, 523,523,523, 523,587,659,0, 523,523,523, 523,587,659,523,440,392, 659,659,659, 523,659,784, 0, 392};
int duration[noteCount]={200,200,200,210,210,275,100,100,100,210,210,250,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,250,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,100,210,210,210,210,210, 210,210,210,210,210,210,210,210,210, 100,210,210,210, 210,210,210,210,210,210, 210,210,210,210,210,210, 210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,100, 210};
//Megalovania
const int noteCount2=122;
int melody2[noteCount2]={294,294,587,0,440,0,415,392,349,294,349,392,294,294,587,0,440,0,415,392,349,294,349,392,247,247,587,0,440,0,415,392,349,294,349, 392, 233,233,587,0, 440, 0, 415,392,349,294, 349,392, 294,294,587,0,440,0,415, 392,349,294,349,392, 262, 262,587, 0, 440,0, 415, 392,349,294,349, 392, 247, 247, 587, 0, 440, 0, 415, 392, 349, 294, 349, 392, 233, 233 , 587, 0,440, 0, 415,392, 349,294,349, 392, 698, 698, 698 ,698, 0, 698, 0, 698,587, 0, 587, 0,698, 698, 698, 698, 0,784, 0, 831, 0,784, 698, 587, 698, 784};// 0, 698, 698, 698, 698,0, 784, 0, 831, 0, 880};
int duration2[noteCount2]={210,210,210,100,210,100,210,210,260,210,210,210,210,210,210,100,210,100,210,210,260,210,210,210,210,210,210,210,210,210,210,260,210,210,210,210,210,210,210,210,210, 210,210,260, 210,210,210,210,210,210,210,210,210,210,210,260,210,210,210, 210,210, 210, 210,210, 210,210, 210, 210, 210, 210, 210,210,210,210, 210,210, 210, 210, 210, 210, 260, 210, 210, 210, 210, 210, 210, 210, 210,210, 210,210,210,210,210, 210, 210, 210, 210, 210, 210, 260,210, 210, 270, 280, 210, 210, 210, 210, 210,210, 210 , 210, 210,210, 210,210, 210,210};// 280, 210 ,210, 210, 210,210,210, 210,210, 210, 210 };

//Concerning Hobbits

const int noteCount3=35;
int melody3[noteCount3]={294, 330, 370, 440, 370,  330, 370,330, 294, 370, 440,  494, 587, 554, 440, 370,392,  370, 329, 440,329,330,294,330,294,  370, 440, 494, 440, 370, 370, 330,294,330, 294};
int duration3[noteCount3]={210, 210, 300, 210,280,  210, 200,210,350,210, 210,   300, 210, 280,210, 280,210,  210, 260,  210,190,210,210,190,300, 210,210,280,210,210, 280,280,210,210,280};




// Star Wars
/*
const int noteCount4=38;
int melody4[noteCount4]={147, 147,147, 0, 196,294, 262, 247, 220, 392, 294, 262, 247, 220, 392, 294, 262, 247, 262, 220,0, 294, 294, 392, 262, 247, 220, 392, 294, 262,247, 220, 392, 294, 262, 247, 262, 220};
int duration4[noteCount4]={210, 210, 210, 210,210, 210, 210, 210,210, 210, 210, 210,210, 210, 210, 210,210, 210, 210, 210,210,210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210};
*/
/*{210,210,210, 210, 300,300, 210, 210, 210, 300, 240, 210,210, 210, 300, 240, 210,210, 210, 240, 220, 210, 240};*/


//Zelda
/*const int noteCount4=28;
int melody4[noteCount4]={262, 196, 196, 262, 233, 208, 233,0, 262, 208, 208, 262, 247, 220, 247, 262, 196, 262, 262, 294, 330, 349, 392,0,  392, 392, 415 ,466, 523};
int duration4[noteCount4]={210,210,210,210,210,210,210,210,210, ,210, 210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210,210};
*/
//Harry Potter
const int noteCount4=61;
int melody4[noteCount4]={247,330,392, 370, 330, 494,440, 370,    330, 392, 370, 311, 350, 247,0 , 247,330,392,370,330,494,587,554,523,415,  523, 494, 466, 233, 392, 330,0, 392, 494, 392, 494, 392, 523,  494, 466,370,392,494,466, 233, 247,494,392,494,  392,494,587,554,523,415,523,494,466, 233,  392,330,};
int duration4[noteCount4]={210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210,210, 210,210,  210, 210, 210,210, 210, 210, 210, 210,210, 210, 210, 210, 210, 210,210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210,  210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210};

//Entrées des boutons
int blackbutton=3;
int yellowbutton=8;
int greenbutton=11;
int purplebutton=5;



//Etats des boutons
int blackbuttonstate=0;
int yellowbuttonstate=0;
int purplebuttonstate=0;
int greenbuttonstate=0;

//Initialisation des variables
int previous=-1;
int score= 0;
short beat=270;
#define melodyPin 12

void setup() {

  pinMode(3,INPUT_PULLUP); 
  pinMode(5,INPUT_PULLUP); 
  pinMode(8,INPUT_PULLUP);
  pinMode(11,INPUT_PULLUP); 
  pinMode(2, OUTPUT );
  pinMode(4, OUTPUT );
  pinMode(7, OUTPUT );
  pinMode(10, OUTPUT);
  pinMode(9, OUTPUT);

 analogWrite(2, 255);
  analogWrite(4, 255);
  analogWrite(7, 255);
  analogWrite(10, 255);
  randomSeed(analogRead(0));
  Serial.begin(9600);
  digitalWrite(9, LOW);

}

void loop() {
//Récupération de l'état des boutons
  blackbuttonstate=digitalRead(blackbutton);
  purplebuttonstate=digitalRead(purplebutton);
  yellowbuttonstate=digitalRead(yellowbutton);
  greenbuttonstate=digitalRead(greenbutton);
 //Initialisation du score 
  score=0;

// Si le bouton noir est appuyé 
  if(blackbuttonstate==LOW){
// Lancement de la boucle de jeu avec pour musique melody
      for (int i = 0; i< noteCount; i++){
    tone(melodyPin,melody[i],duration[i]);
    if(melody[i]!=0){ //Si melody[i] n'est pas un "silence"
    //Choix aléatoire du ruban led a allumer
    long nombre = random(4);
    // Vérification que le même ruban ne soit pas allumée deux fois de suite 
    while(nombre==previous){
      nombre=random(4);
    }
    previous=nombre;
   
    if(nombre==0){
      analogWrite(2,0);  // Allumage de la led 
      delay(beat);
   
   // Boucle testant l'appui du bon bouton 
    for(int i=0; i<30000; i++){
       blackbuttonstate=digitalRead(blackbutton);
       
      if(blackbuttonstate==LOW){
        digitalWrite(9, HIGH);
        score++;
        
        delay(100);
        digitalWrite(9,LOW);
        break;
      }
    }
     noTone(melodyPin);
    analogWrite(2,255);
    }
    //Même principe mais pour le second ruban led 
    if(nombre==1){
      analogWrite(4,0);
      delay(beat);
       for(int i=0; i<30000; i++){
       purplebuttonstate=digitalRead(purplebutton);
      if(purplebuttonstate==LOW){
        digitalWrite(9, HIGH);
        score++;
        delay(100);
        digitalWrite(9,LOW);
        break;
      }
         }
    noTone(melodyPin);
    analogWrite(4,255);
    }
    //Idem
   if(nombre==2){
      analogWrite(7,0);
      delay(beat);
        for(int i=0; i<30000; i++){
       yellowbuttonstate=digitalRead(yellowbutton);
      if(yellowbuttonstate==LOW){
        digitalWrite(9, HIGH);
        score++;
        delay(100);
       digitalWrite(9,LOW);
        break;
      }
         }
    noTone(melodyPin);
  
    analogWrite(7,255);
    }

    //Idem
    if(nombre==3){
      analogWrite(10,0);
      delay(beat);
       for(int i=0; i<30000; i++){
       greenbuttonstate=digitalRead(greenbutton);
      if(greenbuttonstate==LOW){
       digitalWrite(9, HIGH);
        score++;
        delay(100);
        digitalWrite(9,LOW);
        break;
      }
         }
    noTone(melodyPin);
  
    analogWrite(10,255);
    }
    }
  }
  // Affichage du score
   if(score!=0){
  Serial.print("Score :");
  Serial.print(score);
  Serial.print("/");
  Serial.print(noteCount);
  }  
  }
  //Même principe que précédemment mais avec melody2
  if(purplebuttonstate==LOW){
  
    for (int i = 0; i< noteCount2; i++){
    tone(melodyPin,melody2[i],duration2[i]);
    if(melody2[i]!=0){
    long nombre = random(4);
    while(nombre==previous){
      nombre=random(4);
    }
    previous=nombre;
    if(nombre==0){
      analogWrite(2,0);
      delay(beat);
   
   
    for(int i=0; i<30000; i++){
       blackbuttonstate=digitalRead(blackbutton);
       
      if(blackbuttonstate==LOW){
        digitalWrite(9, HIGH);
        score++;
        delay(100);
        digitalWrite(9,LOW);
        break;
      }
    }
     noTone(melodyPin);
    analogWrite(2,255);
    }
    if(nombre==1){
      analogWrite(4,0);
      delay(beat);
       for(int i=0; i<30000; i++){
       purplebuttonstate=digitalRead(purplebutton);
      if(purplebuttonstate==LOW){
        digitalWrite(9, HIGH);
        score++;
        delay(100);
        digitalWrite(9,LOW);
        break;
      }
         }
    noTone(melodyPin);
    analogWrite(4,255);
    }
   if(nombre==2){
      analogWrite(7,0);
      delay(beat);
        for(int i=0; i<30000; i++){
       yellowbuttonstate=digitalRead(yellowbutton);
      if(yellowbuttonstate==LOW){
        digitalWrite(9, HIGH);
        score++;
        delay(100);
       digitalWrite(9,LOW);
        break;
      }
         }
    noTone(melodyPin);
  
    analogWrite(7,255);
    }
    if(nombre==3){
      analogWrite(10,0);
      delay(beat);
       for(int i=0; i<30000; i++){
       greenbuttonstate=digitalRead(greenbutton);
      if(greenbuttonstate==LOW){
        digitalWrite(9, HIGH);
        score++;
        delay(100);
        digitalWrite(9,LOW);
        break;
      }
         }
    noTone(melodyPin);
  
    analogWrite(10,255);
    }
    }
  }
 if(score!=0){
  Serial.print("Score :");
  Serial.print(score);
  Serial.print("/");
  Serial.print(noteCount2);
  }
  }
  //Même principe que précédemment mais avec melody3
  if(yellowbuttonstate==LOW){
      for (int i = 0; i< noteCount3; i++){
    tone(melodyPin,melody3[i],duration3[i]);
    long nombre = random(4);
    while(nombre==previous){
      nombre=random(4);
    }
    previous=nombre;
    if(nombre==0){
      analogWrite(2,0);
      delay(beat);
   
 
    for(int i=0; i<30000; i++){
       blackbuttonstate=digitalRead(blackbutton);
       
      if(blackbuttonstate==LOW){
        digitalWrite(9, HIGH);
        score++;
        delay(100);
        digitalWrite(9,LOW);
        break;
      }
    }
     noTone(melodyPin);
    analogWrite(2,255);
    }
    if(nombre==1){
      analogWrite(4,0);
      delay(beat);
       for(int i=0; i<30000; i++){
       purplebuttonstate=digitalRead(purplebutton);
      if(purplebuttonstate==LOW){
        digitalWrite(9, HIGH);
        score++;
        delay(100);
        digitalWrite(9,LOW);
        break;
      }
         }
    noTone(melodyPin);
    analogWrite(4,255);
    }
   if(nombre==2){
      analogWrite(7,0);
      delay(beat);
        for(int i=0; i<30000; i++){
       yellowbuttonstate=digitalRead(yellowbutton);
      if(yellowbuttonstate==LOW){
        digitalWrite(9, HIGH);
        score++;
        delay(100);
        digitalWrite(9,LOW);
        break;
      }
         }
    noTone(melodyPin);
  
    analogWrite(7,255);
    }
    if(nombre==3){
      analogWrite(10,0);
      delay(beat);
       for(int i=0; i<30000; i++){
       greenbuttonstate=digitalRead(greenbutton);
      if(greenbuttonstate==LOW){
        digitalWrite(9, HIGH);
        score++;
        delay(100);
        digitalWrite(9,LOW);
        break;
      }
         }
    noTone(melodyPin);
  
    analogWrite(10,255);
    }

  }
   if(score!=0){
  Serial.print("Score :");
  Serial.print(score);
  Serial.print("/");
  Serial.print(noteCount3);
  }
  }
  //Même principe que précédemment mais avec melody4
  if(greenbuttonstate==LOW){
      for (int i = 0; i< noteCount4; i++){
    tone(melodyPin,melody4[i],duration4[i]);
    if(melody4[i]!=0){
    long nombre = random(4);
    while(nombre==previous){
      nombre=random(4);
    }
    previous=nombre;
    if(nombre==0){
      analogWrite(2,0);
      delay(beat);
   
   
    for(int i=0; i<31000; i++){
       blackbuttonstate=digitalRead(blackbutton);
       
      if(blackbuttonstate==LOW){
        digitalWrite(9, HIGH);
        score++;
        delay(100);
        digitalWrite(9,LOW);
        break;
      }
    }
     noTone(melodyPin);
    analogWrite(2,255);
    }
    if(nombre==1){
      analogWrite(4,0);
      delay(beat);
       for(int i=0; i<31000; i++){
       purplebuttonstate=digitalRead(purplebutton);
      if(purplebuttonstate==LOW){
        digitalWrite(9, HIGH);
        score++;
        delay(100);
        digitalWrite(9,LOW);
        break;
      }
         }
    noTone(melodyPin);
    analogWrite(4,255);
    }
   if(nombre==2){
      analogWrite(7,0);
      delay(beat);
        for(int i=0; i<31000; i++){
       yellowbuttonstate=digitalRead(yellowbutton);
      if(yellowbuttonstate==LOW){
        digitalWrite(9, HIGH);
        score++;
        delay(100);
        digitalWrite(9,LOW);
        break;
      }
         }
    noTone(melodyPin);
  
    analogWrite(7,255);
    }
    if(nombre==3){
      analogWrite(10,0);
      delay(beat);
       for(int i=0; i<31000; i++){
       greenbuttonstate=digitalRead(greenbutton);
      if(greenbuttonstate==LOW){
      digitalWrite(9, HIGH);
        score++;
        delay(100);
        digitalWrite(9,LOW);
        break;
      }
         }
    noTone(melodyPin);
  
    analogWrite(10,255);
    }
    }
      }
        if(score!=0){
  Serial.print("Score :");
  Serial.print(score);
  Serial.print("/");
  Serial.print(noteCount4);
  }
  }
  }
