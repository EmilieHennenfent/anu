Téléverser le code .ino sur l'Arduino


    Nombre de joueurs : 1 à 4
    Un joueur appuie sur l'un des quatre boutons pour choisir une mélodie
    Les joueurs se répartissent le contrôle des boutons
    Chaque joueur doit appuyer sur son ou ses boutons lorsque les LEDs associées s'allument
    Lorsqu'un bouton est appuyé au bon moment, la diode verte s'allume
    La partie prend fin lorsque la musique s'arrête
    Le score obtenu s'affiche sur l'ordinateur

